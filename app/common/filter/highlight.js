'user strict';

angular.module('ntwforum').filter('highlight', function ($sce) {
    return function (text, seatchTerm) {
        if (seatchTerm) text = text.replace(new RegExp('(' + seatchTerm + ')', 'gi'),
            '<span class="highlighted">$1</span>')

        return $sce.trustAsHtml(text)
    }
});