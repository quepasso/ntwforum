'use strict';

angular.module('forumModule')
    .component('forumResults', {
        templateUrl: 'forum/view/forum-results.template.html',
        controller: ['$scope', '$location', 'Search',
            function results($scope, $location, Search) {
                var query = (typeof($location.search()['query']) === 'undefined') ? 'NO' : $location.search()['query'];
                var answer = (typeof($location.search()['answer']) === 'undefined') ? 'NO' : $location.search()['answer'];

                        $scope.results = Search.search({
                            'search': $location.search()['search'],
                            'query': query,
                            'answer': answer
                        });
            }]
    });