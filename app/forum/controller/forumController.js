'use strict';

angular.module('forumModule')
    .controller('ForumController', ['$scope', 'Forum', '$uibModal', 'Flash',
        function ($scope, Forum, $uibModal, Flash) {

        }
    ])
    .controller('ForumAddController', ['$rootScope', '$scope', 'Forum', 'items', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, items, $uibModalInstance, Flash) {
            $scope.ok = function () {
                var newPost = this.post;
                Forum.save(newPost).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ])
    .controller('ForumEditController', ['$rootScope', '$scope', 'Forum', 'items', 'id', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, items, id, $uibModalInstance, Flash) {
            $scope.item = Forum.get({'id': id});
            $scope.ok = function () {
                var updatedPost = { title: this.item.post.title, body: this.item.post.body };
                Forum.update({'id': this.item.post.id}, updatedPost).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ])
    .controller('ForumRemoveController', ['$rootScope', '$scope', 'Forum', 'items', 'id', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, items, id, $uibModalInstance, Flash) {
            $scope.item = Forum.get({'id': id});
            $scope.ok = function () {
                Forum.remove({'id': this.item.post.id}, this.item.post).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ])
    .controller('ForumAnswerController', ['$rootScope', '$scope', 'Forum', 'Comment', 'items', 'id', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, Comment, items, id, $uibModalInstance, Flash) {
            $scope.item = Forum.get({'id': id});
            $scope.ok = function () {
                var newComment = this.comment;
                newComment.post_id = this.item.post.id;
                Comment.save( newComment ).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ])
    .controller('ForumEditAnswerController', ['$rootScope', '$scope', 'Forum', 'Comment', 'items', 'id', 'rep_id', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, Comment, items, id, rep_id, $uibModalInstance, Flash) {
            $scope.item = Comment.get({'id': rep_id});
            $scope.ok = function () {
                var updatedComment = { message: this.item.comment.message };
                Comment.update({id: this.item.comment.id}, updatedComment ).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ])
    .controller('ForumRemoveAnswerController', ['$rootScope', '$scope', 'Forum', 'Comment', 'items', 'id', 'rep_id', '$uibModalInstance', 'Flash',
        function ($rootScope, $scope, Forum, Comment, items, id, rep_id, $uibModalInstance, Flash) {
            $scope.item = Comment.get({'id': rep_id});
            $scope.ok = function () {
                Comment.delete({id: this.item.comment.id}).$promise.then(
                    function( result ){
                        var id = Flash.create(result.level, result.message, 3000, true);
                        $uibModalInstance.close();
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        $uibModalInstance.close();
                    }
                )
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ]);
