'user strict';
// Déclaration du service 'Comment'
angular.
    module('forumModule').
    factory('Comment', ['$resource',
        function ($resource) {
            return $resource('http://ntwforum/api/webroot/index.php/api/comments/:id.json',
            {},
            {
                query: {
                    method: 'GET',
                        skipAuthorization: true
                },
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE'
                }
            });
        }
    ]);
