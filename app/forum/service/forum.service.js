'use strict';
// Déclaration du service 'Forum'
angular.
    module('forumModule').
    factory('Forum', ['$resource',
        function ($resource) {
            return $resource('http://ntwforum/api/webroot/index.php/api/posts/:id.json',
            {},
            {
                query: {
                    method: 'GET',
                    skipAuthorization: true
                },
                update: {
                    method: 'PUT'
                },
                delete: {
                    method: 'DELETE'
                }
            });
        }
    ]);