'use strict';
// Déclaration du service 'Search'
angular.
module('forumModule').
factory('Search', ['$resource',
    function ($resource) {
        return $resource('http://ntwforum/api/webroot/index.php/api/posts/search.json',
            {
                search: '@search',
                query: '@query',
                answer: '@answer'},
            {
                search: {
                    method: 'GET',
                    params: { 'search': '@search', 'query': '@query', 'answer': '@answer' }
                }
            });
    }
]);