'use strict';
// Composant 'forumItems', avec son template et son contrôleur
angular
    .module('forumModule').
    component('forumItems', {
        templateUrl: 'forum/view/forum-items.template.html',
        controller: ['$scope', 'Forum', '$sessionStorage', '$uibModal', 'Flash',
            function forumItemsController($scope, Forum, $sessionStorage, $uibModal, Flash) {
                var self = this;
                self.items = Forum.query();
                $scope.open = function (id, rep_id, action) {
                    var modalInstance = $uibModal.open({
                        backdrop: true,
                        keyboard: true,
                        templateUrl: 'forum/view/template/forum-' + action + '.template.html',
                        controller: 'Forum' + action + 'Controller',
                        resolve: {
                            id: function () {
                                return id;
                            },
                            rep_id: function () {
                                return rep_id;
                            },
                            action: function () {
                                return action;
                            },
                            items: function () {
                                return $scope.items;
                            }
                        }
                    });
                    modalInstance.result.then(function () {
                        self.items = Forum.query();
                    }, function () {
                        var id = Flash.create('warning', 'Annulation de l\'opération',2000,true);
                    });
                }
                self.storage = $sessionStorage;
                self.sortKey = '-created';
                self.sort = function (keyname) {
                    self.sortKey = keyname;
                    self.reverse = !self.reverse;
                }
            }
        ]
    });