'use strict';
// Composant 'forumSearch', avec son template et son contrôleur
angular
    .module('forumModule').
component('forumSearch', {
    templateUrl: 'forum/view/forum-search.template.html',
    controller: ['$scope', '$location',
        function forumSearchController($scope, $location) {
            $scope.search = function (search, query, answer) {
                if (!query && !answer) {
                    query = 'YES';
                }
                $location.path('/forum/search').search({'search': search, 'query': query, 'answer': answer });
            }
        }
    ]
});