'use strict';

angular.
    module('forumModule').
    component('forumItem', {
        templateUrl: 'forum/view/forum-item.template.html',
        controller: ['$rootScope', '$scope', '$routeParams', 'Forum', 'Comment', '$sessionStorage', '$uibModal', 'Flash',
            function ForumItemController($rootScope, $scope, $routeParams, Forum, Comment, $sessionStorage, $uibModal, Flash) {
                var self = this;
                self.item = Forum.get({id: $routeParams.id }).$promise.then(
                    function (result) {
                        self.item.post = result.post;
                    },
                    function( error ){
                        var message = '<strong>Erreur ' + error.data.code +' : </strong>' + error.data.message + '!';
                        var id = Flash.create('danger', message, 3000, true);
                        window.location = '/app/#';
                    }
                );
                $scope.open = function (id, rep_id, action) {
                    var modalInstance = $uibModal.open({
                        backdrop: true,
                        keyboard: true,
                        templateUrl: 'forum/view/template/forum-' + action + '.template.html',
                        controller: 'Forum' + action + 'Controller',
                        resolve: {
                            id: function () {
                                return id;
                            },
                            rep_id: function () {
                                return rep_id;
                            },
                            action: function () {
                                return action;
                            },
                            items: function () {
                                return $scope.items;
                            }
                        }
                    });
                    modalInstance.result.then(function () {
                        self.items = Forum.query();
                        if ((action === 'Edit') || (action === 'Answer') || (action === 'EditAnswer') || (action === 'RemoveAnswer')) {
                            self.item = Forum.get({id: id});
                        }
                        if (action === 'Remove') {
                            window.location = '/app/#';
                        }
                    }, function () {
                        var id = Flash.create('warning', 'Annulation de l\'opération',3000,true);
                    });
                }
            }]
})