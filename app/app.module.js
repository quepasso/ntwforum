'use strict';

// Déclaration du module principal 'ntwForum'
angular.module('ntwforum', [
    'forumModule',
    'userModule',
    'ngSanitize',
    'ngResource',
    'ngStorage',
    'ngRoute',
    'angular-jwt',
    'angularUtils.directives.dirPagination',
    'ngFlash',
    'ui.bootstrap'
]);
