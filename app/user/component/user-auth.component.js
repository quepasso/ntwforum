'use strict';
// Composant 'userAuth', avec son template et son contrôleur
angular
    .module('userModule')
    .component('userAuth', {
    templateUrl: 'user/view/user-auth.template.html',
    controller: ['$rootScope', '$scope', 'User', 'AuthService', '$sessionStorage', 'Flash', 'jwtHelper', '$uibModal',
        function userAuthController($rootScope, $scope, User, AuthService, $sessionStorage, Flash, jwtHelper, $uibModal) {
            $scope.storage = $sessionStorage;
            if ($sessionStorage['token']) {

                if(jwtHelper.isTokenExpired($sessionStorage['token'])) {
                    $sessionStorage.$reset();
                    var message = 'Expiration du jeton d\'authentification.';
                    var id = Flash.create('success', message,0,true);
                    window.setTimeout(function(){location.reload()},1000);
                } else {
                    $rootScope.expirationDate = jwtHelper.getTokenExpirationDate($sessionStorage['token']);
                    $rootScope.payload = jwtHelper.decodeToken($sessionStorage['token']);
                }
            }
            $scope.login = function () {
                AuthService.token({'username': this.user.username, 'password': this.user.password}, function (result) {
                    $scope.storage.token = result.token;
                    var message = 'Authentification effectuée avec succès!';
                    var id = Flash.create('success', message,3000,true);
                    window.setTimeout(function(){location.reload()},1000);
                }, function () {
                    var message = '<strong>Erreur de connexion : </strong>utilisateur ou mot de passe incorrect!';
                    var id = Flash.create('danger', message,3000,true);
                })
            }
            $scope.logout = function () {
                $sessionStorage.$reset();
                $rootScope.expirationDate = null;
                $rootScope.payload = null;
                var message = 'Vous êtes maintenant déconnecté.';
                var id = Flash.create('success', message,3000,true);
                window.setTimeout(function(){location.reload()},1000);
            }
            $scope.register = function () {
                var modalInstance = $uibModal.open({
                    backdrop: true,
                    keyboard: true,
                    templateUrl: 'user/view/template/user-add.template.html',
                    controller:  'RegisterController',
                    resolve: {}
                });
                modalInstance.result.then(function () {
                }, function () {
                    var message = 'Annulation de l\'opération!';
                    var id = Flash.create('warning', message,3000,true);
                });
            }
        }
    ]
});