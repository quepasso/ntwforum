'use strict';
// Déclaration du service 'AuthService'
angular.
    module('userModule')
    .factory('AuthService', ['$resource',
        function ($resource) {
            return $resource('http://ntwforum/api/webroot/index.php/api/users/token/:user/:password.json',
                {},
                {
                    token: {
                        method: 'POST'
                    }
                },
                {
                    stripTrailingSlashes: false
                }
            );
    }
    ]);
