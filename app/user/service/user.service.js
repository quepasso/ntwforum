'use strict';
// Déclaration du service 'core.user'
angular.
module('userModule').
factory('User', ['$resource',
    function ($resource) {
        return $resource('http://ntwforum/api/webroot/index.php/api/users/:id.json',
            {},
            {
                query: {
                    method: 'GET'
                }
            });
    }
]);