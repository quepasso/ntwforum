angular.module('userModule')
    .controller('RegisterController', ['$scope', 'User', '$uibModal', 'Flash', '$uibModalInstance',
        function ($scope, User, $uibModal, Flash, $uibModalInstance) {
            $scope.ok = function () {
                var newUser = this.user;
                newUser.role = 'user';
                newUser.active = 1;
                User.save(newUser, function (result) {
                    console.log(result.message);
                    var message = result.message + '<br /><strong>Token : </strong>' + result.token;
                    var id = Flash.create(result.level, message, 3000, true);
                    $uibModalInstance.close();
                })
            }
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    ]);