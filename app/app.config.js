'use strict';
angular.
    module('ntwforum').
    config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider
                .hashPrefix('!')
                .html5Mode(false);
            $routeProvider.
                when('/forum/search', {
                    template: '<forum-results></forum-results>'
            }).
                when('/forum', {
                    template: '<forum-items></forum-items>'
            }).
                when('/forum/:id', {
                    template: '<forum-item></forum-item>'
            }).

            otherwise('/forum');
        }
    ])
    .config(function Config($httpProvider, jwtOptionsProvider) {
        // Please note we're annotating the function so that the $injector works when the file is minified
        jwtOptionsProvider.config({
            tokenGetter: ['$sessionStorage', function($sessionStorage) {
                return $sessionStorage['token'];
            }]
        });
        $httpProvider.interceptors.push('jwtInterceptor');
    });