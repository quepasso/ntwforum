# ntwForum — CAS Nouvelles Technologies du Web - UNI GE
Le forum a l'architecture suivante:

* Client => AngularJS
* REST Service => CakePhp
* Base de données => MySQK

## Principales fonctionnalités ##
* Pour pouvoir proposer une nouvelle question ou répondre à une question existante, les utilisateurs doivent être connecté.
* L'utilisateur connecté peut créer une nouvelle question, modifier ou supprimer les questions dont il est l'auteur.
* L'utilisateur connecté peut répondre à une question, modifier ou supprimer les réponses dont il est l'auteur.
* Une fonction de recherche permet de chercher dans le titre et le corps des questions et/ou des réponses.

### Authentification ###
L'authentification est de type "Stateless". A la connexion, un jeton d'authentification de type JWT est crée pour une durée déterminée. Ce jeton est transmis via les entêtes http lors de l'utilisation des services nécessitant une autorisation. La vérification se fait du coté serveur. Une fois connecté, l'utilisateur peur vérifier, dans le menu en haut à droite de l'interface,  l'heure d'expiration de son jeton ainsi que son rôle. Il a également la possibilité d'y faire une dé connexion.

### Questions et réponses ###
Afin de proposer une nouvelle question, l'utilisateur doit être connecté. Du coté client de l'application, le bouton "Ajouter une question" n'est affiché que si l'id de l'utilisateur est présent dans les $rootscope (extrait du jeton d'authentification => payload.sub). La vérification finale se fait bien entendu de coté serveur dans le cas ou un utilisateur non connecté arriverait à poster une question. La vérification sur la modification et suppression de questions se fait sur le même principe. Dans la partie client, les boutons "Modifier" et "Supprimer" ne sont affichés que si l'id de l'utilisateur connecté est égale à celui qui a créé la question. La vérification finale se fait du coté serveur.

Les vérifications sur les réponses se font exactement de la manière.

### Recherche ###
La recherche peut se faire sur le titre et le corps de la question et/ou sur le contenu des réponses. A l'affichage du résultat, le ou les mots recherché(s) sont surlignés en jaune pour une meilleure lisibilité.

## Installation

### Clone du dépôt:
```
git-clone https://git@bitbucket.org:quepasso/ntwforum.git
cd ntwforum
```

### Installation les dépendances pour ce projet:

```
npm install
```
### Installation des composants bower:
```
bower install
```

Téléchargement manuel du gh-pagees
```
wget https://github.com/angular-ui/bootstrap/archive/gh-pages.zip

```
Il faut ensuite décompresser l'archive et copier le répertoire bootstrap-gh-pages dans le répertoire bower_components du projet.
Importation de la base de données. Faite une copie du fichier api/config/app.default.php en app.php et mettre à jour les informations de connexion de la base. 
Il y a deux utilisateurs créés:

* utilisateur
* admin

Tous deux ont comme mot de passe: password

## Arborescence du projet
Composition générale de l'arborescence du projet

```
api/                    --> Service REST CakePHP
  src/
    Controller/         --> Contrôleurs du service REST
    Model/              --> Modèles du service RES
app/                    --> Partie client Angularjs
  app.css               --> default stylesheet
  assets/               --> Feuille de styles + librairie js hors angularjs
  bower_components/     --> Comopsants bower
  common/               --> Fichiers communs
    filter/             --> Filtres communs à l'application
  forum/
    component/          --> Composants du module forum
    controller/         --> Controleurs du module forum
    service/            --> Services du module forum
    view/               --> Vues du module forume
    forum.module.js     --> Module Forum
  user/
    component/          --> Composants du module user
    controller/         --> Controleurs du module user
    service/            --> Services du module user
    view/               --> Vues du module user
    user.module.js      --> Module user
  app.module.js         --> Module principal de l'application
  app.config.js         --> Fichier de configuration principal
bower.json              --> Fichier de configuration bower
package.json            --> Fichier de configuration des packages
restForum.pdf           --> Modèle de la base de données
restForum.sql           --> Export de la base de données
    
```

## Liens utiles ##

* Angular: http://angularjs.org/
* Git: http://git-scm.com/
* Bower: http://bower.io
* Npm: https://www.npmjs.org/
* Node: http://nodejs.org
* Bitbucket: https://bitbucket.org