-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Lun 26 Septembre 2016 à 07:57
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `restForum`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `message` text,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`id`, `message`, `user_id`, `post_id`, `created`, `modified`) VALUES
(1, 'In hac habitasse platea dictumst. Praesent egestas neque eu enim. Curabitur suscipit suscipit tellus. In hac habitasse platea dictumst. Praesent egestas neque eu enim. Curabitur suscipit suscipit tellus. Modification ok!', 28, 62, '2016-09-26 05:40:22', '2016-09-26 05:41:57'),
(2, 'Praesent ac sem eget est egestas volutpat. Pellentesque auctor neque nec urna. Phasellus dolor. Cras dapibus.\nPraesent ac sem eget est egestas volutpat. Pellentesque auctor neque nec urna. Phasellus dolor. Cras dapibus. Recherche.', 28, 62, '2016-09-26 05:42:23', '2016-09-26 05:43:13');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `body` text NOT NULL,
  `views` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `views`, `created`, `modified`) VALUES
(62, 28, 'Vestibulum ante ipsum primis recherche.', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Ut varius tincidunt libero. Morbi mattis ullamcorper velit. Sed hendrerit. Etiam rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Ut varius tincidunt libero. Morbi mattis ullamcorper velit. Sed hendrerit. Etiam rhoncus.\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Ut varius tincidunt libero. Morbi mattis ullamcorper velit. Sed hendrerit. Etiam rhoncus. Et ça marche?', 0, '2016-09-26 05:38:42', '2016-09-26 05:43:30'),
(63, 28, 'Donec quam felis ultricies nec.', 'Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Donec venenatis vulputate lorem. Sed lectus. Nullam accumsan lorem in dui. Phasellus tempus.', 0, '2016-09-26 05:42:48', '2016-09-26 05:42:48');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `username`, `role`, `active`, `created`, `modified`) VALUES
(28, 'utilisateur@local.ch', '$2y$10$hzdKUB4LWDoHkvOpy6KRYuMkhWAulqZHEDVB0oLzgHxPpNQQ9EE9e', 'utilisateur', 'user', 1, '2016-09-26 05:38:00', '2016-09-26 05:38:00'),
(29, 'admin@local.ch', '$2y$10$4kPDjiJhoekwwBCe5sxgu.koF71qKkXIbmAKeNfhn6l150ys01wIG', 'admin', 'admin', 1, '2016-09-26 05:56:54', '2016-09-26 05:56:54');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_key` (`post_id`),
  ADD KEY `user_key` (`user_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_key` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
