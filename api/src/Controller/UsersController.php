<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['add', 'token', 'view', 'find']);
    }

       /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->Users->find('all');
        $this->set(compact('users'));
        $this->set('_serialize', ('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
    /**
     * Find method
     *
     * @param string|null $username User username.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function find($username = null)
    {
        $user = $this->Users->find()->select('id')->where(['username' => $username], [
            'contain' => []
        ]);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $level = 'success';
                $message = 'L\'utilisateur a été enregistré!';
                $id = $user['id'];
                $token = JWT::encode(
                    [
                        'sub' => $user['id'],
                        'name' => $user['name'],
                        'exp' =>  time() + 1800
                    ],
                    Security::salt());

            } else {
                $level = 'danger';
                if($user->errors('email')) {
                    $error = $user->errors('email');
                    $message = $error['uniqueEmail'];

                } else {
                    $message = 'Not ok';
                }
            }
        }
        $this->set([
            'message'   => $message,
            'level'     => $level,
            'id'        => $id,
            'token'     => $token
        ]);
        $this->set(compact('user', 'level', 'message', 'id', 'token'));
        $this->set('_serialize', ['user', 'level', 'message', 'id', 'token']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data());
            if ($this->Users->save($user)) {
                $message = "L'utilisateur ". $user['username'] ." a bien a été mis jour.";
            } else {
                $message = "L'utilisateur ". $user['username'] ." ne peut pas être mis à jour.";
            }
        }
        $this->set(compact('user', 'level', 'message'));
        $this->set('_serialize', ['user', 'level', 'message']);
    }

        /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $level = 'success';
            $message = "L'utilisateur ". $user['username'] ." a bien été supprimé correctement";
        } else {
            $level = 'danger';
            $message = "L'utilisateur ". $user['username'] ." n'a pas été supprimé, réssayer plus tard";
        }
        $this->set(compact('user', 'level', 'message'));
        $this->set('_serialize', ['user', 'level', 'message']);
    }

    public function token()
    {
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException('Invalid username or password');
        }
        $this->Auth->setUser($user);
        $this->set([
            'id' => $user['id'],
            'role' => $user['role'],
            'message' => true,
                'token' => JWT::encode([
                    'sub'   => $user['id'],
                    'name'  => $user['username'],
                    'role'  => $user['role'],
                    'exp'   =>
                        time() + 1800
                ],
                    Security::salt())

        ]);
    }
}
