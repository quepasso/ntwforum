<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['index', 'view', 'search']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        //$posts = $this->paginate($this->Posts);

        $posts = $this->Posts->find('all', [
            'contain' => ['Comments', 'Users']
        ]);
        $this->set(compact('posts'));
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Comments.Users', 'Users']
        ]);
        $this->set(compact('post'));

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->data);
            $post->user_id = $this->Auth->user('id');
            if ($this->Posts->save($post)) {
                $level = 'success';
                $message = 'La question a été ajoutée';
            } else {
                $level = 'danger';
                $message = 'Impossible d\'ajouter la question.';
            }
        }
        $this->set(compact('post', 'level', 'message'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->data);
            if ($this->Posts->save($post)) {
                $level = 'success';
                $message = 'La question a été sauvegardée';
            } else {
                $level = 'danger';
                $message = 'Impossible de sauvegarder la question!';
            }
        }
        $this->set(compact('post', 'level', 'message'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $level = 'success';
            $message = 'La question a été supprimée';
        } else {
            $level = 'danger';
            $message = 'Impossible de supprimer la question';
        }
        $this->set(compact('post', 'level', 'message'));
    }

    public function search()
    {
        $search = $this->request->query('search');
        $query = $this->request->query('query');
        $answer = $this->request->query('answer');
        if (($query === 'YES') && ($answer === 'YES')) {
            $comment = $this->Posts->find()
                ->select(['title', 'body'])
                ->matching(
                    'Comments', function(\Cake\ORM\Query $q) {
                    return $q
                        ->select(['Comments.message'])
                        ->orWhere(['Comments.message like ' => '%'. $this->request->query('search') .'%']);
                });
            $post = $this->Posts->find()
                ->where(['title like' => '%'. $search .'%'])
                ->orWhere(['body like' => '%'. $search .'%']);
            $this->set(compact('post', 'comment', 'search', 'query', 'answer'));

        }  if  (($answer === 'YES') && ($query === 'NO')) {
           $comment = $this->Posts->find()
                ->select(['title', 'body'])
                ->matching(
                    'Comments', function(\Cake\ORM\Query $q) {
                    return $q
                        ->select(['Comments.message'])
                        ->orWhere(['Comments.message like ' => '%'. $this->request->query('search') .'%']);
                });
            $this->set(compact('comment', 'search', 'query', 'answer'));
        } if (($answer === 'NO') && ($query === 'YES')) {
            $post = $this->Posts->find()
                ->where(['title like' => '%'. $search .'%'])
                ->orWhere(['body like' => '%'. $search .'%']);
            $this->set(compact('post', 'search', 'query', 'answer'));
        }
    }
    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent ajouter des articles
        if ($this->request->action === 'add') {
            return true;
        }

        // Le propriétaire d'un article peut l'éditer et le supprimer
        if (in_array($this->request->action, ['edit', 'delete'])) {
            $postId = (int)$this->request->params['pass'][0];
            if ($this->Posts->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }
}
